//OS Lab8 task 4 - getrusage
//by Gosha Stepanov @gosha2st 20191009
//Compilation parameters:
//gcc -std=c11 ex4.c -o q -O0 -Wall -pedantic

#include <stdio.h>
#include <stdlib.h>		//for malloc()
#include <string.h>		//for memset()
#include <unistd.h>		//for sleep()
#include <sys/resource.h>	//for getrusage()

int main(){
	printf("OS Lab8 task 4 - getrusage()\n");
	printf("----------------------------------------------------------------------------------------------------------\n");
	printf("| Maximum Resident set size | User CPU time | System CPU time | Page faults (w/o I/O) | Context switches |\n");
	printf("----------------------------------------------------------------------------------------------------------\n");
	struct rusage usage;
	unsigned long bytes = 1024 * 1024 * 10;
	int *ptr[10];
	unsigned char i=0;
	for(i; i<10; ++i){
		ptr[i]=(int*)malloc(bytes);
		if(ptr[i]==NULL){
			printf("Error occured - cannot allocate next 10MB.\nProgram terminated.\n");
			return 1;
		}
		memset(ptr[i], 0, bytes);
		getrusage(RUSAGE_SELF, &usage);
		printf("| %25ld | %13ld | %15ld | %21ld | %16ld |\n", usage.ru_maxrss, usage.ru_utime.tv_usec, usage.ru_stime.tv_usec, usage.ru_minflt, usage.ru_nvcsw + usage.ru_nivcsw);
		sleep(1);
	}
	for(i=0; i<10; ++i)
		free(ptr[i]);
	printf("----------------------------------------------------------------------------------------------------------\n");
	return 0;
}