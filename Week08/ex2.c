//OS Lab8 task 2 - 10 MB
//by Gosha Stepanov @gosha2st 20191009
//Compilation parameters:
//gcc -std=c11 ex2.c -o q -O0 -Wall -pedantic

#include <stdio.h>
#include <stdlib.h>	//for malloc()
#include <string.h>	//for memset()
#include <unistd.h>	//for sleep()

int main(){
	unsigned long bytes = 1024 * 1024 * 10;
	int *ptr[10];
	unsigned char i=0;
	for(i; i<10; ++i){
		ptr[i]=(int*)malloc(bytes);
		if(ptr[i]==NULL){
			printf("Error occured - cannot allocate next 10MB.\nProgram terminated.\n");
			return 1;
		}
		memset(ptr[i], 0, bytes);
		sleep(1);
	}
	for(i=0; i<10; ++i)
		free(ptr[i]);
	return 0;
}