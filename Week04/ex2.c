//OS Lab4 task 2 - Fork loop
//by Gosha Stepanov @gosha2st 20190911
//Compilation parameters:
//gcc -std=c11 -Wall -pedantic ex2.c -o q -O0

//#include <zconf.h>
//#include <sys/types.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(){
	for(unsigned char i=0; i<3; ++i){
		int pid=fork();
		printf("%d\n",pid);
		sleep(1);
		system("pstree -p user");
	}
	
	return 0;
}