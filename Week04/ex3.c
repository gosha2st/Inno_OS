//OS Lab4 task 3 - Simplistic shell
//by Gosha Stepanov @gosha2st 20190911
//Compilation parameters:
//gcc -std=c11 -Wall -pedantic ex3.c -o q -O0

#include <stdio.h>
#include <stdlib.h>

int main(void) {
	char command[10];
	while(1){
		printf("> ");
		scanf("%s", command);
		system(command);
	}
	return 0;
}