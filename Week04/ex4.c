//OS Lab4 task 4 - Extended shell
//by Gosha Stepanov @gosha2st 20190911
//Compilation parameters:
//gcc -std=c11 -Wall -pedantic ex4.c -o q -O0

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

int main(void) {
	char command[10];
	printf("> ");
	fgets(command, sizeof(command), stdin);
	while (strcmp(command, "exit\n") != 0) {
		printf("> ");
		system(command);
		fgets(command, sizeof(command), stdin);
		printf("\n");
	}
	return 0;
}