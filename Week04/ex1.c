//OS Lab4 task 1 - Forks
//by Gosha Stepanov @gosha2st 20190911
//Compilation parameters:
//gcc -std=c11 -Wall -pedantic ex1.c -o q -O0

//#include <zconf.h>
//#include <stdlib.h>
//#include <sys/types.h>

#include <stdio.h>
#include <unistd.h>

int main(void){
	int n=42;
	int pid=fork();
	if(pid>0)
		printf("Hello from parent [%d - %d]\n", pid, n);
	else
		printf("Hello from child [%d - %d]\n", pid, n);
	
	return 0;
}

/*
Output after running the program 10 times:

Hello from parent [3492 - 42]
Hello from child [0 - 42]
Hello from parent [3604 - 42]
Hello from child [0 - 42]
Hello from parent [3606 - 42]
Hello from child [0 - 42]
Hello from parent [3608 - 42]
Hello from child [0 - 42]
Hello from parent [3610 - 42]
Hello from child [0 - 42]
Hello from parent [3612 - 42]
Hello from child [0 - 42]
Hello from parent [3614 - 42]
Hello from child [0 - 42]
Hello from parent [3616 - 42]
Hello from child [0 - 42]
Hello from parent [3618 - 42]
Hello from child [0 - 42]
Hello from parent [3620 - 42]
Hello from child [0 - 42]

We see that each time 2 strings are printed: one from parent process and one from child.
Meanwhile, parent process's id increases by 2 each time, while child process's id always remains zero.
*/