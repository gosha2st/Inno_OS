//OS Lab2 task 5 - Extended Triangle
//by Gosha Stepanov @gosha2st 20190828
//Compilation parameters:
//gcc -std=c11 -Wall ex5.c -o q.exe -O0

#include <stdio.h>

unsigned short n=0;

//pattern functions:

void one(){
	for(unsigned short i=0;i<n;++i){
		for(unsigned short j=0;j<n;++j)
			printf("*");
		printf("\n");
	}
}

void two(){
	for(unsigned short i=0;i<n;++i){
		for(unsigned short j=0;j<=i;++j)
			printf("*");
		printf("\n");
	}
}

void three(){
	for(unsigned short i=n;i>0;--i){
		for(unsigned short j=0;j<i;++j)
			printf("*");
		printf("\n");
	}
}

void four(){
	unsigned short row, pos;
	for(row=1;row<=n;++row){
		for(pos=0;pos<n-row;++pos)
			printf(" ");
		for(pos=0;pos<2*row-1;++pos)
			printf("*");
		printf("\n");
	}
}

void five(){
	for(unsigned short i=0;i<n;++i){
		for(unsigned short j=0;j<=i;++j)
			printf("*");
		printf("\n");
	}
	for(unsigned short i=n-1;i>0;--i){
		for(unsigned short j=0;j<i;++j)
			printf("*");
		printf("\n");
	}
}

int main(unsigned char argc, char *argv[]){
	printf("This program prints different triangles composed of * based on pattern you choose.\n");
	if(argc!=2){	//if number of additional command line arguments does not equal to 1:
		printf("Incorrect number of command line arguments!\n");
		printf("Put exactly 1 additional argument n to define the number of rows to be printed.\n");
		printf("Program terminated.");
		return 1;
	}
	char i;
	for(i=0;argv[1][i]!='\0';++i)	//convert the string given as additional argument to a number num:
		switch(argv[1][i]){
			case '0':
				n*=10;
				break;
			case '1':
				n*=10;
				++n;
				break;
			case '2':
				n*=10;
				n+=2;
				break;
			case '3':
				n*=10;
				n+=3;
				break;
			case '4':
				n*=10;
				n+=4;
				break;
			case '5':
				n*=10;
				n+=5;
				break;
			case '6':
				n*=10;
				n+=6;
				break;
			case '7':
				n*=10;
				n+=7;
				break;
			case '8':
				n*=10;
				n+=8;
				break;
			case '9':
				n*=10;
				n+=9;
				break;
			default:	//in case of another (forbidden) symbol:
				printf("Incorrect format of command line argument!\n");
				printf("Put exactly 1 additional argument n to define the number of rows to be printed.\n");
				printf("Program terminated.");
				return 1;
		}
	printf("Enter one digit between 1 and 5 to specify the pattern for printing: ");
	scanf("%c",&i);
	while((i!='1')&&(i!='2')&&(i!='3')&&(i!='4')&&(i!='5')){	//check input for correctness:
		printf("Incorrect pattern number! Enter one digit between 1 and 5 to specify the pattern: ");
		scanf("%c",&i);
	}
	switch(i){	//select appropriate pattern:
		case '1':
			one();
			break;
		case '2':
			two();
			break;
		case '3':
			three();
			break;
		case '4':
			four();
			break;
		case '5':
			five();
			break;
		default:
			printf("Unexpected error occured. Program terminated.");
			return 1;
	}
			
		
	return 0;
}