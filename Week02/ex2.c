//OS Lab2 task 2 - String reverse
//by Gosha Stepanov @gosha2st 20190828
//Compilation parameters:
//gcc -std=c11 -Wall ex2.c -o q.exe -O0

#include <stdio.h>

int main(){
	char s[100];
	printf("Enter string to be reversed (100 characters max):\n");
	fgets(s, 100, stdin);
	printf("Reversed string:");
	unsigned char i;
	for(i=0;s[i]!='\0';++i){}
	for(--i;i!=0;--i)
		printf("%c",s[i]);
	printf("%c",s[i]);
	
	return 0;
}