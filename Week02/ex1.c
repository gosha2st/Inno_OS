//OS Lab1 task 1 - Variables by Gosha Stepanov @gosha2st 20190828
//Compilation parameters:
//gcc -std=c11 -Wall ex1.c -o q.exe -O0

#include <stdio.h>
#include <limits.h>
#include <float.h>

int main(){
	int i = INT_MAX;
	float f = FLT_MAX;
	double d = DBL_MAX;
	printf("INT_MAX = %d, size = %d\n", i, sizeof(i));
	printf("FLT_MAX = %f, size = %d\n", f, sizeof(f));
	printf("DBL_MAX = %lf, size = %d\n", d, sizeof(d));
		
	return 0;
}