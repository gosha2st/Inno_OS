//OS Lab2 task 3 - Triangle
//by Gosha Stepanov @gosha2st 20190828
//Compilation parameters:
//gcc -std=c11 -Wall ex3.c -o q.exe -O0

#include <stdio.h>

int main(unsigned char argc, char *argv[]){
	printf("This program prints a right-side-up triangle of height n and width 2n-1 composed of *\n");
	if(argc!=2){	//if number of additional command line arguments does not equal to 1:
		printf("Incorrect number of command line arguments!\n");
		printf("Put exactly 1 additional argument n to define the number of rows to be printed.\n");
		printf("Program terminated.");
		return 1;
	}
	unsigned char i;
	unsigned short n=0;
	for(i=0;argv[1][i]!='\0';++i)	//convert the string given as additional argument to a number num:
		switch(argv[1][i]){
			case '0':
				n*=10;
				break;
			case '1':
				n*=10;
				++n;
				break;
			case '2':
				n*=10;
				n+=2;
				break;
			case '3':
				n*=10;
				n+=3;
				break;
			case '4':
				n*=10;
				n+=4;
				break;
			case '5':
				n*=10;
				n+=5;
				break;
			case '6':
				n*=10;
				n+=6;
				break;
			case '7':
				n*=10;
				n+=7;
				break;
			case '8':
				n*=10;
				n+=8;
				break;
			case '9':
				n*=10;
				n+=9;
				break;
			default:	//in case of another (forbidden) symbol:
				printf("Incorrect format of command line argument!\n");
				printf("Put exactly 1 additional argument n to define the number of rows to be printed.\n");
				printf("Program terminated.");
				return 1;
		}
	//main algorithm for printing:
	unsigned short row, pos;
	for(row=1;row<=n;++row){
		for(pos=0;pos<n-row;++pos)
			printf(" ");
		for(pos=0;pos<2*row-1;++pos)
			printf("*");
		printf("\n");
	}
		
	return 0;
}