//OS Lab2 task 4 - Integers Swap
//by Gosha Stepanov @gosha2st 20190828
//Compilation parameters:
//gcc -std=c11 -Wall ex4.c -o q.exe -O0

#include <stdio.h>

void swap(int *ap,int *bp){
	int t=*ap;
	*ap=*bp;
	*bp=t;
}

int main(){
	printf("Enter integer number 1: ");
	int a, b;
	int *ap=&a, *bp=&b;
	scanf("%d",&a);
	printf("Enter integer number 2: ");
	scanf("%d",&b);
	swap(ap,bp);
	printf("Swapped number 1: %d, number 2: %d",a,b);
	return 0;
}