#include <stdio.h>
#include <sys/types.h>
#include <dirent.h>
#include <string.h>
#include <sys/stat.h>

int main(){
	DIR* dirp=opendir("tmp");	//open "tmp" directory
	if(dirp==NULL){			//if fails:
		printf("Error occured - directory not found.\nProgram terminated.");
		return -1;
	}
	char fn[64];
	struct dirent *ent;
	struct stat st;
	
	//read one-by-one:
	while((ent=readdir(dirp)) != NULL){
		//remove "." and "..":
		if((strcmp(ent->d_name, "."))&&(strcmp(ent->d_name, ".."))){
			//create full file name for stat:
			strcpy(fn, "tmp");
			strcat(fn, "/");
			strcat(fn, ent->d_name);
			
			//calculate number of hard links:
			stat(fn, &st);
			
			//select those which have at least 2 hard links:
			if(st.st_nlink > 1)
				printf("File name: %s, link #%ld\n", ent->d_name, ent->d_ino);
		}
	}
	return 0;
}