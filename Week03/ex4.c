//OS Lab3 task 4 - Quicksort
//by Gosha Stepanov @gosha2st 20190904
//Compilation parameters:
//gcc -std=c11 -Wall ex4.c -o q.exe -O0

#include <stdio.h>

void quickSort(int ar[], int first, int last){	//sorts array using Quickort algorithm:
	if(first<last){		//first parameter is array itself;
		int left=first;
		int right=last;
		int mid=ar[(left+right)/2];	//pivot
		do{
			while(ar[left]<mid)
				++left;
			while(ar[right]>mid)
				--right;
			if(left<=right){
				int tmp=ar[left];
				ar[left]=ar[right];
				ar[right] = tmp;
				++left;
				--right;
			}
		}while(left<=right);
		quickSort(ar, first, right);
		quickSort(ar, left, last);
	}
}

int main(){
	int ar[8]={124,6,124,4,53, -0, 0, -11};	//example
	unsigned char length=sizeof(ar)/sizeof(int);
	printf("Initial array:\t\t");
	for(unsigned char i=0; i<length; ++i)
		printf("%d ", ar[i]);
	printf("\n");
	quickSort(ar, 0, length-1);
	printf("quickSort() applied:\t");
	for(unsigned char i=0; i<length; ++i)
		printf("%d ", ar[i]);
	
	return 0;
}