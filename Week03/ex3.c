//OS Lab3 task 3 - Linked List
//by Gosha Stepanov @gosha2st 20190904
//Compilation parameters:
//gcc -std=c11 -Wall ex3.c -o q.exe -O0

#include <stdio.h>
#include <stdlib.h>	//for malloc()

struct Node{	//carcass for node:
	int data;	//node's main data - integer
	struct Node *next;	//pointer to the next node
};

struct Node *head=NULL;	//global pointer to the head of the list; empty initially

struct Node *createNode(int data){	//creates node based on data, and returns pointer to the new node
	struct Node *node = malloc(sizeof(struct Node));	//creating node
	(*node).data=data;		//assigning the value
	(*node).next=NULL;		//next is NULL initially
	printf("Node with value %d created.\n", (*node).data);
	return node;
}

void printList(){	//prints values of all nodes in the list:
	if(head==NULL){	//if list is empty:
		printf("List is empty.\n");
		return;
	}
	printf("Printing list: ");
	struct Node *curr=head;
	while(curr!=NULL){		//if list not empty, traverse it:
		printf("%d ", (*curr).data);	//print the value of a node
		curr=(*curr).next;		//go to next node
	}
	printf("\n");
}

void insertNode(struct Node *tbi, struct Node *node_p){	//inserts node tbi after node node_p:
	(*tbi).next=(*node_p).next;
	(*node_p).next=tbi;
	printf("Node with value %d inserted after node with value %d.\n", (*tbi).data, (*node_p).data);
}

void deleteNode(struct Node *tbd){		//deletes node tbd from the list and returns its value:
	struct Node *curr=head;
	struct Node *prev=NULL;
	while((curr!=NULL)&&(curr!=tbd)){
		prev=curr;
		curr=(*curr).next;
	}
	if(curr==tbd){
		if(curr==head){
			int data=(*head).data;
			head=(*head).next;
			free(curr);
			printf("Node (head) with value %d deleted.\n", data);
			return;
		}
		(*prev).next=(*curr).next;
		int data=(*curr).data;
		free(curr);
		printf("Node with value %d deleted.\n", data);
		return;
	}
	printf("Node not found! Nothing was deleted.\n");
}

int main(){
	printList();		//list is empty initially
	head=createNode(5);	//create new node with value 5 and make it the head of the list
	printList();
	insertNode(createNode(2),head);	//create new node with value 2 and insert it after head
	printList();
	insertNode(createNode(-1),(*head).next);	//create new node with value -1 and insert it as a third elem
	printList();
	insertNode(createNode(-10),head);	//create new node with value -10 and insert it after head
	printList();
	deleteNode((*head).next);		//delete the second node
	printList();
	deleteNode(head);		//delete the head
	printList();
	
	return 0;
}