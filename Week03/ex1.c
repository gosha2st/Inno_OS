//OS Lab3 task 1 - Compile and run
//by Gosha Stepanov @gosha2st 20190904
//Compilation parameters:
//gcc -std=c11 -Wall ex1.c -o q.exe -O0

#include <stdio.h>

int main(){
	int* pc;
	int c;
	c=22;
	printf("Address of c:%d\n",&c);
	printf("Value of c:%d\n\n",c);
	pc=&c;
	printf("Address of pointer pc:%d\n",pc);
	printf("Content of pointer pc:%d\n\n",*pc);
	c=11;
	printf("Address of pointer pc:%d\n",pc);
	printf("Content of pointer pc:%d\n\n",*pc);
	*pc=2;
	printf("Address of c:%d\n",&c);
	printf("Value of c:%d\n\n",c);
	
	return 0;
}