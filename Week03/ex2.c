//OS Lab3 task 2 - bubbleSort()
//by Gosha Stepanov @gosha2st 20190904
//Compilation parameters:
//gcc -std=c11 -Wall ex2.c -o q.exe -O0

#include <stdio.h>

void bubbleSort(int a[], unsigned short length){
	int tmp;
	unsigned short i, j;
	for(i=0; i<length; ++i)
		for(j=0; j<length-i-1; ++j)
			if(a[j+1]<a[j]){
				tmp=a[j];
				a[j]=a[j+1];
				a[j+1]=tmp;
			}
}

int main(){
	int a[8]={124,6,124,4,53, -0, 0, -11};	//example
	unsigned short length=sizeof(a)/sizeof(int);
	printf("Initial array:\t\t");
	for(unsigned char i=0; i<length; ++i)
		printf("%d ", a[i]);
	printf("\n");
	bubbleSort(a, length);
	printf("bubbleSort() applied:\t");
	for(unsigned char i=0; i<length; ++i)
		printf("%d ", a[i]);
		
	return 0;
}