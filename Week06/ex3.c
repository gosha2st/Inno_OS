//OS Lab6 task 3 - SIGINT Signal
//by Gosha Stepanov @gosha2st 20190925
//Compilation parameters:
//gcc -std=c11 ex3.c -o q.exe -O0 -Wall -pedantic

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <signal.h>
#include <windows.h>

unsigned char flag=1;

void handler(int i){
	printf("Signal received! Execution stopped.\n", i);
	flag=0;
}

int main(){
	signal(SIGINT, handler);
	while(flag){
		printf("I'm working...\n");
		Sleep(500);
	}
	return 0;
}