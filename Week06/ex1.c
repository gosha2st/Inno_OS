//OS Lab6 task 1 - Pipe
//by Gosha Stepanov @gosha2st 20190925
//Compilation parameters:
//gcc -std=c11 ex1.c -o q.exe -O0 -Wall -pedantic

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>

int main(void){
	char s1[] = "Message";
	char s2[10];
	int fd[2];
	pipe(fd);
	printf("String 2 before: %s\n", s2);
	write(fd[1], s1, 10);
	read(fd[0], s2, 10);
	close(fd[0]);
	close(fd[1]);
	printf("String 2 after: %s\n", s2);
	return 0;
}