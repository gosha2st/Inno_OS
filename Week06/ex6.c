//OS Lab6 task 6 - Check descriprion
//by Gosha Stepanov @gosha2st 20190925
//Compilation parameters:
//gcc -std=c11 ex6.c -o q -O0 -Wall -pedantic

#include <signal.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdlib.h>

int main(){
	int pid1=0;

	//create pipe between Parent and Child1:
	int fd[2];
	pipe(fd);
	
	printf("Parent: Creating Child1...\n");
	if(pid1 = fork()){		//Parent:
		int pid2=0;
		printf("Parent: Child1's id is: %d\n", pid1);
		printf("Parent: Creating Child2...\n");
		if(pid2=fork()){	//Parent:
			printf("Parent: Sending the message via pipe...\n");
			write(fd[1], &pid2, sizeof(pid2));		//write Child2's id to the pipe
			int status = -999;
			printf("Parent: Wait for a child...\n");
			waitpid(pid2, &status, 0);
			printf("Parent: Something happened. Child2's. Status is: %d\n", status);
			exit(0);
		}else{		//Child2:
			printf("Child2: id = %d\n", getpid());
			while(1){
				printf("Child2: I'm waiting...\n");
				sleep(2);
			}
		}
	}else{			//Child1:
		printf("Child1: id = %d\n", getpid());
		sleep(2);
		int pid_c2 = 0;
		read(fd[0], &pid_c2, sizeof(pid_c2));	//read message from the pipe
		printf("Child1: Child2's id received: %d\n", pid_c2);
		printf("Child1:  Sending SIGSTOP...\n");
		kill(pid_c2, SIGSTOP);		
		exit(0);
	}
	return 0;
}

//Description is false since when we run the program, it waitpids for the Child2 even if SIGSTOP is sent.