//OS Lab6 task 2 - Fork Pipe
//by Gosha Stepanov @gosha2st 20190925
//Compilation parameters:
//gcc -std=c11 ex2.c -o q -O0 -Wall -pedantic

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>

int main(){
	unsigned char pid=0;
	int fd[2];
	pipe(fd);
	if(pid=fork()){	//parent:
		char s1[10] = "Message";
		write(fd[1], s1, 10);
	}
	else{		//child:
		char s2[10];
		printf("String in the child process before reading from pipe:\n%s\n", s2);
		read(fd[0], s2, 10);
		printf("String in the child process after reading from pipe:\n%s\n", s2);
	}
	return 0;
}