//OS Lab6 task 5 - SIGTERM
//by Gosha Stepanov @gosha2st 20190925
//Compilation parameters:
//gcc -std=c11 ex5.c -o q -O0 -Wall -pedantic

#include <signal.h>
#include <stdio.h>
#include <unistd.h>

int main(){
	int pid=0;
	if(pid=fork()){	//parent:
		printf("Parent here. Child created. Wait for 10 seconds...\n");
		sleep(10000);
		kill(pid ,SIGTERM);
		printf("Child killed. My job is done.\n");
	}else{		//child:
		while(1){
			printf("I'm alive...\n");
			sleep(2);
		}
	}
	return 0;
}