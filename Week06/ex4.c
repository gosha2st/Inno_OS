//OS Lab6 task 4 - SIGKILL, SIGSTOP and SIGUSR1
//by Gosha Stepanov @gosha2st 20190925
//Compilation parameters:
//gcc -std=c11 ex4.c -o q -O0 -Wall -pedantic

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <signal.h>
#include <windows.h>

void handler_kill(){
	printf("Terminated because of *kill*\n");
	exit(0);
}

void handler_stop(){
	printf("Terminated because of *stop*\n");
	exit(0);
}

void handler_usr1(){
	printf("Terminated because of *usr1*\n");
	exit(0);
}

int main(){
	signal(SIGKILL, handler_kill);
	signal(SIGSTOP, handler_stop);
	signal(SIGUSR1, handler_usr1);
	while(1){
		printf("I'm working...\n");
		sleep(2);
	}
	return 0;
}