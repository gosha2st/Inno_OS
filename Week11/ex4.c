//OS Lab11 task 4 - Memory mappings
//by Gosha Stepanov @gosha2st 20191031
//Compilation parameters:
//gcc -std=c11 ex4.c -o ex4 -O0 -Wall -pedantic
  
#include <stdio.h>
#include <fcntl.h>		//for open()
#include <sys/types.h>		//additional library for open()
#include <sys/stat.h>		//for fstat()
#include <string.h>		//for memcpy()
#include <sys/mman.h>		//for mmap()
#include <unistd.h>		//for ftruncate()

#define SOURCE "ex1.txt"
#define TARGET "ex1.memcpy.txt"

//This program copies content of SOURCE to TARGET using memory mappings

int main(){
	printf("\nThis program copies content of \"%s\" to \"%s\"\nusing memory mappings.\n",SOURCE,TARGET);
	printf("\nOpening \"%s\"...\n",SOURCE);
	int in=open(SOURCE,O_RDONLY);
	if(in==-1){
		printf("Failed to open \"%s\"! Program terminated.\n",SOURCE);
		return 1;
	}
	else
		printf("Opened successfully.\n");
	printf("\nCreating and opening \"%s\"...\n",TARGET);
	int out=open("ex1.memcpy.txt", O_RDWR | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR);
	if(out==-1){
		printf("Failed to create or open \"%s\"! Program terminated.\n",TARGET);
		return 1;
	}
	else
		printf("Created and opened successfully.\n");
	printf("\nGetting \"%s\" file stat...\n",SOURCE);
	struct stat fst;
	if(fstat(in,&fst)==-1){
		printf("Failed to get file stat! Program terminated.\n");
		return 1;
	}
	printf("Stat got successfully: st_size = %ld\n",fst.st_size);
	if(fst.st_size==0){
		printf("Nothing to copy. Program finished.\n");
		return 0;
	}
	printf("\nTruncate \"%s\" to this size using ftruncate()...\n",TARGET);
	if(ftruncate(out, fst.st_size)!=0){
		printf("Failed to truncate \"%s\"! Program terminated.\n",TARGET);
		return 1;
	}
	printf("Truncated successfully.\n");
	printf("\nCreating mmap for both files...\n");
	char* src=mmap(NULL, fst.st_size, PROT_READ, MAP_PRIVATE, in, 0);
	char* dst=mmap(NULL, fst.st_size, PROT_READ | PROT_WRITE, MAP_SHARED, out, 0);
	if(src==MAP_FAILED||dst==MAP_FAILED){
		printf("Failed to create mmap! Program terminated.\n");
		return 1;
	}
	printf("mmaps created successfully.\n");
	printf("\nCopy content of size %ld from \"%s\" to \"%s\" using memcpy()...\n", fst.st_size, SOURCE, TARGET);
	memcpy(dst, src, fst.st_size);
	printf("Copied successfully!\n");
	printf("\nClosing \"%s\" and \"%s\"...\n",SOURCE,TARGET);
	close(in);
	close(out);
	printf("Program finished.\n");
	return 0;
}