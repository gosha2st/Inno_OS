//OS Lab11 task 2 - Line buffer
//by Gosha Stepanov @gosha2st 20191031
//Compilation parameters:
//gcc -std=c11 ex2.c -o ex2 -O0 -Wall -pedantic

#include <stdio.h>
#include <unistd.h>	//for sleep()

#define STRING_LENGTH 6
char *string="Hello\n";

int main(){
	char buff[STRING_LENGTH];
	setvbuf(stdout, buff, _IOLBF, sizeof buff);
	for(unsigned short i=0; i<STRING_LENGTH; ++i){
		printf("%c", string[i]);
		sleep(1);
	}
	return 0;
}