//OS Lab11 task 1 - This is a nice day
//by Gosha Stepanov @gosha2st 20191031
//Compilation parameters:
//gcc -std=c11 ex1.c -o ex1 -O0 -Wall -pedantic

#include <stdio.h>
#include <fcntl.h>		//for open()
#include <sys/types.h>		//additional library for open()
#include <sys/stat.h>		//for fstat()
#include <string.h>		//for memcpy()
#include <sys/mman.h>		//for mmap()
#include <unistd.h>		//for ftruncate()

int main(){
	int fd=open("ex1.txt", O_RDWR);
	if(fd==-1){
		printf("Failed opening \"ex1.txt\". Program terminated.\n");
		return 1;
	}
	struct stat fst;
	if(fstat(fd, &fst)==-1){
		printf("Failed to get file stat. Program terminated.\n");
		return 1;
	}
	char *map=mmap(NULL, fst.st_size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
	if(map==MAP_FAILED){
		printf("Failed to create a mmap. Program terminated.\n");
		return 1;
	}
	ftruncate(fd, 18);
	memcpy(map, "This is a nice day", 19);
	close(fd);
	return 0;
}