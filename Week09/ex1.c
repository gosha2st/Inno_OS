//OS Lab9 task 1 - Paging system using the ageing algorithm.
//by Gosha Stepanov @gosha2st 20191016
//Compilation parameters:
//gcc -std=c11 ex1.c -o q.exe -O0 -Wall -pedantic

#include <stdio.h>
#include <stdlib.h>	//for calloc()

#define MAX_PAGES 1000
#define AGE_LIMIT 28

char filename[50]="input.txt";		//can be changed during runtime
unsigned char frameFound = 0;	//boolean
unsigned char pf, frameIdx = 0, oldestFrameIdx = 0;
unsigned short hits = 0, misses = 0, reference;

int main(){
	printf("This program simulates a paging system using the ageing algorithm.\n");
	printf("Maximum page number equals 1000; and age limit equals 28 since int is used to simulate memory.\n");
	printf("Enter the number of page frames as a nonnegative integer < 255: ");
	scanf("%hu", &pf);
	FILE *file_input;
	while((file_input=fopen(filename, "r")) == NULL){
		printf("Cannot open \"%s\". Enter the input file name: ", filename);
		scanf("%s", filename);
	}
	printf("Reading from file \"%s\"...\n", filename);
	
	int *memory=calloc(pf, sizeof(int));
	int *refCount = calloc(MAX_PAGES, sizeof(int));
	while(!feof(file_input)){
		if(fscanf(file_input,"%hu",&reference)!=1)
			break;
		frameFound = 0;
		for(frameIdx = 0; ((frameIdx<pf)&&(frameFound==0)); ++frameIdx)
			if(reference==memory[frameIdx]){
				++hits;
				frameFound=1;
			}
		if(!frameFound){
			++misses;
			oldestFrameIdx = 0;
			int leastCounter = refCount[memory[oldestFrameIdx]];
			for(frameIdx = 1; frameIdx < pf; ++frameIdx){
				if(leastCounter>refCount[memory[frameIdx]]){
					oldestFrameIdx = frameIdx;
					leastCounter = refCount[memory[frameIdx]];
				}
			}
			memory[oldestFrameIdx] = reference;
		}

		for(int i=0; i<MAX_PAGES; ++i){
			refCount[i] >>= 1;
			refCount[i] |= (reference == i) << AGE_LIMIT;
		}
	}
		
	printf("Hits:\t\t%hu\n", hits);
	printf("Misses:\t\t%hu\n", misses);
	printf("Hit/miss ratio: %f\n", (float)hits / misses);
	return 0;
}