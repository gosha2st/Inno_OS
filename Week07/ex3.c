//OS Lab7 task 3 - Code template
//by Gosha Stepanov @gosha2st 20191002
//Compilation parameters:
//gcc -std=c11 ex3.c -o q.exe -O0 -Wall -pedantic

#include <stdio.h>
#include <stdlib.h>	//for malloc(), realloc() and free()

int main(){
	printf("\nThe purpose of this program is to create an initial array of a user-specified size\n");
	printf("and then dynamically resize the array to a new user-specified size.\n");
	printf("\nEnter original array size: ");
	unsigned short orig, final;
	unsigned char i;
	scanf("%hu",&orig);
	printf("\nCreating array of integers with original length %hu...\n",orig);
	short *arr=malloc(orig*sizeof(short));
	printf("Array ceated. Initializing the array with incremental values starting from 0...\n");
	for(i=0; i<orig; ++i)
		arr[i]=i;
	printf("All elements initialized. Print the array: ");
	for(i=0; i<orig; ++i)
		printf("%d ",arr[i]);
	printf("\nNow specify the new array size: ");
	scanf("%hu",&final);
	if(final>orig){
		printf("\nNew size is larger than the original one. Resizing the array...\n");
		realloc(arr, final*sizeof(short));
		printf("Array's size changed. Initializing all new elements with incremental values starting from 0...\n");
		for(i=orig; i<final; ++i)
			arr[i]=i-orig;
	}else{
		if(final==orig)
			printf("\nNew array's size is equal to the original. No actions needed.\n");
		else{
			printf("\nNew array's size is smaller than the original. Resizing the array...\n");
			realloc(arr,final*sizeof(short));
			printf("\nArray's size changed. Lost of data detected!\n");			
		}
	}
	printf("Print new array: ");
	for(i=0; i<final; ++i)
		printf("%d ",arr[i]);
	printf("\nDeallocating the memory...\n");
	free(arr);
	printf("Memory is free again!\nEnd of program.");
	return 0;
}