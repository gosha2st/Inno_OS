//OS Lab7 task 4 - Your own realloc()
//by Gosha Stepanov @gosha2st 20191002
//Compilation parameters:
//gcc -std=c11 ex4.c -o q.exe -O0

#include <stdio.h>
#include <stdlib.h>	//for malloc() and free()

void *realloc_my(void *ptr_old, size_t size_old, size_t size_new){
	if(ptr_old==NULL){
		ptr_old=malloc(size_new);
		return ptr_old;
	}
	if(size_new==0){
		free(ptr_old);
		return NULL;
	}
	if(size_old==size_new)
		return ptr_old;
	
	void *ptr_new=malloc(size_new);	//create new empty array of the new size specified by user
	
	//copy data manually from old array to the new one:
	size_t min;
	if(size_old>size_new)
		min=size_new;
	else
		min=size_old;
	unsigned short i=0;
	for(i; i<min; ++i)
		((unsigned short*)ptr_new)[i]=((unsigned short*)ptr_old)[i];		//???
	
	free(ptr_old);
	ptr_old=ptr_new;
	return ptr_old;
}

int main(){
	printf("This program implements my own realloc_my() function using only malloc() and free().\n");
	printf("realloc_my() changes the size of the memory block pointed to by the new size.\n");
	printf("In fact it creates new array with the new size and copies old data into it manually i.e. w/o memcpy().\n");
	printf("The function accepts 3 arguments - pointer to the old array, its size, and the new desirable size.\n");
	printf("And returns a new pointer to the new array with the new size and data copied from the old array.\n");
	printf("The content is unchanged to the minimum of the old and new sizes.\n");
	printf("Newly allocated memory is uninitialized.\n");
	printf("If ptr is NULL, the call is equivalent to malloc(size).\n");
	printf("If new size is zero, the call is equivalent to free(ptr).\n\n");
	
	printf("Here is an example:\n");
	printf("Let's create an array of unsigned short with length 10 using call \"realloc_my(NULL, NULL, 10*sizeof(short))\"...\n");	unsigned short *arr=realloc_my(NULL, NULL, 10*sizeof(short));
	printf("Print the array: ");
	unsigned short i=0;
	for(i; i<10; ++i)
		printf("%hu ",arr[i]);
	printf("\nInitialize the array with incremental values starting from 0...\n");
	for(i=0; i<10; ++i)
		arr[i]=i;
	printf("Print the array: ");
	for(i=0; i<10; ++i)
		printf("%hu ",arr[i]);
	printf("\nCall function \"realloc_my(arr, 10, 10)\"; Nothing should change:\n");
	arr=realloc_my(arr, 10, 10);
	printf("Print the array: ");
	for(i=0; i<10; ++i)
		printf("%hu ",arr[i]);
	printf("\nChange (increase) size of the array from 10 to 20 by calling \"realloc_my(arr, 10, 20)\"...\n");
	arr=realloc_my(arr, 10, 20);
	printf("Print the array: ");
	for(i=0; i<20; ++i)
		printf("%hu ",arr[i]);
	printf("\nChange (decrease) size of the array from 20 to 5 by calling \"realloc_my(arr, 20, 5)\"...\n");
	arr=realloc_my(arr, 20, 5);
	printf("Print the array: ");
	for(i=0; i<5; ++i)
		printf("%hu ",arr[i]);
	printf("\nDelete the array (clear the memory) by calling \"realloc_my(arr, 5, 0)\"...\n");
	arr=realloc_my(arr, 5, 0);
	printf("Array deleted. Memory is free!\n");
	printf("End of program.");
	
	return 0;
}