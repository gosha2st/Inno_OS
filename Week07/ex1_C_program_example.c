//OS Lab6 task 8 - Scheduling algorithm
//by Gosha Stepanov @gosha2st 20190926
//Compilation parameters:
//gcc -std=c11 ex1_C_program_example.c -o ex1_C_program_example.exe -O0 -Wall -pedantic

#include <stdio.h>
#include <windows.h>	//for Sleep()

int main(){
	printf("This is implementation of a simple scheduling algorithm ");
	printf("that picks the easiest (unsigned charest) job every time.\n");
	printf("Enter number of processes (jobs): ");
	unsigned char num, i, j;
	scanf("%hu", &num);
	
	//create and initialize two-dimensional array,
	//0'th index of which is Process number, and 1'st - CPU burst (time needed to complete):
	unsigned char processes[2][num];
	for(i=0; i<num; ++i){
		processes[0][i]=i+1;
		processes[1][i]=i+1;
	}
	
	for(i=0; i<num; ++i){
		printf("Enter CPU burst (i.e. number of CPU cycles needed to compete) for process %hu: ", i+1);
		scanf("%hu", &processes[1][i]);
	}
	Sleep(300);
	printf("Creating processes execution schedule...\n");
	//sort two-dimensional array of processes in ascending run-time using Insertion sort:
	unsigned char value, temp;
	for(i=1; i<num; ++i){
		value=processes[1][i];
		temp=processes[0][i];
		for(j=i; j>0&&processes[1][j-1]>value; --j){
			processes[0][j]=processes[0][j-1];
			processes[1][j]=processes[1][j-1];
		}
		processes[1][j]=value;
		processes[0][j]=temp;
	}
	Sleep(2000);
	printf("Schedule created! Starting execution according to planned schedule...\n");
	Sleep(1000);
	//do actual calculations on CPU according to planned schedule:
	for(i=0; i<num; ++i){
		printf("P%hu", processes[0][i]);
		Sleep(1000);
		for(j=0; j<processes[1][i]; ++j){
			printf("=");
			Sleep(500);
		}
	}
	Sleep(500);
	printf("\nI'm done!");
	return 0;
}