//OS Lab7 task 2 - Dynamical memory allocation
//by Gosha Stepanov @gosha2st 20191002
//Compilation parameters:
//gcc -std=c11 ex2.c -o q.exe -O0 -Wall -pedantic

#include <stdio.h>
#include <stdlib.h>	//for malloc() and free()

int main(){
	printf("\nThis program dynamically allocates memory for an array of N integers,\n");
	printf("fills the array with incremental values starting from 0, prints the array\n");
	printf("and deallocates the memory.\n\n");
	printf("Enter number N of integers to be stored in a dynamic array: ");
	unsigned char n, i;
	scanf("%hu", &n);
	printf("\nCreating dynamical array of integers of length %hu...\n",n);
	int *arr=(int*)malloc(n*sizeof(int));
	if(arr==NULL){
		printf("\nFatal error - cannot allocate the memory!\nProgram terminated.\n");
		return 1;
	}
	printf("\nMemory allocated. Initializing the array with incremental values starting from 0...\n");
	for(i=0; i<n; ++i)
		arr[i]=i;
	printf("\nArray initialized. Printing the array: ");
	for(i=0; i<n; ++i)
		printf("%d ", arr[i]);
	printf("\n\nDeallocating the memory...\n");
	free(arr);
	printf("Memory is free again!\nEnd of program.");
	return 0;
}