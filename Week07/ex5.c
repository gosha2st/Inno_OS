//OS Lab7 task 5 - Find and fix
//by Gosha Stepanov @gosha2st 20191002
//Compilation parameters:
//gcc -std=c11 ex5.c -o q.exe -O0 -Wall -pedantic

#include <stdio.h>

int main(){
	char *st[1]; 		
	char **s = st;
	char foo[] = "Hello World";
	
	*s = foo;
	printf("s is %s\n",*s);

	s[0] = foo;
	printf("s[0] is %s\n",s[0]);
	return 0;
}