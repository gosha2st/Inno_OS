//OS Lab12 task 1 - /dev/random
//by Gosha Stepanov @gosha2st 20191106
//Compilation parameters:
//gcc -std=c11 ex1.c -o ex1 -O0 -Wall -pedantic

#include <stdio.h>

#define SIZE 20

int main(){
	printf("\nThis program gets 20 random symbols from /dev/random\n");
	printf("and puts them into new \"ex1.txt\".\n");
	printf("\nOpening /dev/random...\n");
	FILE *in=fopen("/dev/random", "r");
	if(in==NULL){
		printf("Failed opening \"/dev/random\". Program terminated.\n");
		return 1;
	}
	printf("\nCreating \"ex1.txt\"...\n");
	FILE *out=fopen("ex1.txt", "w");
	if(out==NULL){
		printf("Failed creating \"ex1.txt\". Program terminated.\n");
		return 1;
	}
	char buffer[SIZE+1];
	buffer[SIZE]='\0';
	printf("\nReading 20 symbols from /dev/random into buffer...\n");
	fgets(buffer, SIZE+1, in);
	printf("\nWriting buffer to \"ex1.txt\" char-by-char...\n");
	for(unsigned char i=0; i<20; ++i)
		fprintf(out, "%c", buffer[i]);
	printf("\nProgram finished. Check the output in \"ex1.txt\".\n");
	return 0;
}