//OS Lab12 task 2 - The tee command
//by Gosha Stepanov @gosha2st 20191107
//Compilation parameters:
//gcc -std=c11 ex2.c -o ex2 -O0 -Wall -pedantic

#include <stdio.h>
#include <stdlib.h>		//for malloc()

void tee(int argc, char *argv[]){	//implementation of tee command with annotations in a command line:
	unsigned char i=0;
	unsigned char append=0;
	if((argc>1) && (argv[1][0]=='-') && (argv[1][1]=='a') && (argv[1][2]=='\0')){
		append=1;
		printf("\nAppend mode: on\n");
	}
	else
		printf("Append mode: off\n");
	unsigned char numFiles = argc - append - 1;
	FILE **files=malloc(numFiles*sizeof(FILE*));
	if(files==NULL){
		printf("\nError allocating memory using malloc(). Program terminated.\n");
		return;
	}
	const char *mode=append ? "a" : "w";
	printf("\nOpening files given as command-line arguments...\n");
	for(i;i<numFiles;++i){
		files[i]=fopen(argv[i+append+1], mode);
		if(files[i]==NULL){
			printf("Error opening \"%s\". Program terminated.\n", argv[i+append+1]);
			return;
		}
	}
	char buffer[128];
	printf("\nReading from input and writing into opened files and stdout...\n");
	while((fgets(buffer,127,stdin)!=NULL) && (buffer[0]!='\n')){
		printf("%s", buffer);
		for(i=0;i<numFiles;++i)
			fprintf(files[i], "%s", buffer);
	}
	printf("Reading and writing finished. Closing opened files...\n");
	for(i=0;i<numFiles;++i)
		fclose(files[i]);
	printf("Files closed. Check the output.\n");
	free(files);
	printf("Temporary memory free. Program finished.\n\n");
}

int main(int argc, char *argv[]){
	printf("\nThis program implements tee command using I/O system calls.\n");
	tee(argc, argv);
}