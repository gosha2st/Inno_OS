//OS Lab4 task 1 - Threads
//by Gosha Stepanov @gosha2st 20190918
//Compilation parameters on Windows:
//gcc -std=c11 ex1.c -o q.exe -O0 -Wall -pedantic -lpthread

#include <stdio.h>
#include <pthread.h> 	//for threads
#include <windows.h>	//for Sleep()

unsigned char n=0;
void *thread_print(){
	printf("Thread number %d greets you\n",n++);
}

int main(){
	//Parallel threading (no threads wait for others):
	printf("Parallel Threading:\n");
	pthread_t tid;
	for(unsigned char i=0; i<4; ++i){
		printf("Thread number %d is created\n",n);
		pthread_create(&tid, NULL, thread_print, NULL);
	}
	Sleep(500);	//one half of a second
	printf("\n");

	//Sequential threading (every thread waits previous to complete):
	printf("Sequential Threading:\n");
	for(unsigned char j=0; j<4; ++j){
		printf("Thread number %d is created\n",n);
		pthread_create(&tid, NULL, thread_print, NULL);
		pthread_join(tid, NULL);
	}
	return 0;
}


//Output:
/*
Parallel Threading:
Thread number 0 is created
ThrTehad nrumber e0a is credat ed
nuTmhTread nuhmbebrr eer1a  d0is creat greets y ou
ed
nuThread number 2m Tbghreadrer  numbe1 greets eyour
2 is createed
ts yoThread number u
3 greets you

Sequential Threading:
Thread number 4 is created
Thread number 4 greets you
Thread number 5 is created
Thread number 5 greets you
Thread number 6 is created
Thread number 6 greets you
Thread number 7 is created
Thread number 7 greets you
*/